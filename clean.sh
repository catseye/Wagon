#!/bin/sh

# SPDX-FileCopyrightText: Chris Pressey, the original author of this work, has dedicated it to the public domain.
# For more information, please refer to <https://unlicense.org/>
# SPDX-License-Identifier: Unlicense

rm -f src/*.hi src/*.o src/*.jsmod bin/*.exe
rm -f src/Language/Wagon/*.hi src/Language/Wagon/*.o src/Language/Wagon/*.jsmod
